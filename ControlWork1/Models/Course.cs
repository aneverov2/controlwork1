﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork1.Models
{
    public class Course : Entity
    {
        public string Name { get; set; }

        public ICollection<StudentCourses> StudentCourses { get; set; }
        public Course()
        {
            StudentCourses = new List<StudentCourses>();
        }
    }
}

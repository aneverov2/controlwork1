﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork1.Models
{
    public class Student : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<StudentCourses> StudentCourses { get; set; }
        public Student()
        {
            StudentCourses = new List<StudentCourses>();
        }
    }
}

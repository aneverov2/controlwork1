﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork1.Models
{
    public class StudentWithCourses
    {
        public Student Student { get; set; }
        public List<Course> BaseCourses{ get; set; }
        public List<StudentCourses> Courses { get; set; }
    }
}

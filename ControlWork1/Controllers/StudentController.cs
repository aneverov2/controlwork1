﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork1.Data.Repositories.Contracts;
using ControlWork1.Models;
using Microsoft.AspNetCore.Mvc;

namespace ControlWork1.Controllers
{
    public class StudentController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public StudentController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IActionResult Index()
        {
            List<Student> students = _unitOfWork.StudentRepository.GetAll().ToList();
            return View(students);
        }
        public IActionResult Create(int id)
        {
            if (id == 0)
            {
                return View();
            }

            Student student = _unitOfWork.StudentRepository.GetById(id);
            return View(student);

        }
        public IActionResult CreateOrUpdate(Student student)
        {
            _unitOfWork.StudentRepository.Add(student);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            Student student = _unitOfWork.StudentRepository.GetById(id);
            return View(student);
        }

        public IActionResult DeleteItem(Student student)
        {
            _unitOfWork.StudentRepository.Delete(student);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            StudentWithCourses model = new StudentWithCourses();
            Student student = _unitOfWork.StudentRepository.GetById(id);
            List<Course> baseCourses = _unitOfWork.CourseRepository.GetAll().ToList();
            List<StudentCourses> courses = _unitOfWork.StudenCoursesRepository.GetStudentCourses(id).ToList();
            model.Student = student;
            model.Courses = courses;
            model.BaseCourses = baseCourses;
            return View(model);
        }

        public IActionResult AddToCourse(int studentId, int courseId)
        {
            if (courseId == 0)
            {
                return RedirectToAction("Details", new { id = studentId });
            }
            StudentCourses courses = new StudentCourses();
            Student student = _unitOfWork.StudentRepository.GetById(studentId);
            Course course = _unitOfWork.CourseRepository.GetById(courseId);
            courses.Course = course;
            courses.CourseId = courseId;
            courses.Student = student;
            courses.StudentId = studentId;
            _unitOfWork.StudenCoursesRepository.Add(courses);
            if (!_unitOfWork.StudenCoursesRepository.IsExist(studentId, courseId))
            {
                _unitOfWork.Save();
            }
            else
            {
                _unitOfWork.RejectChanges();
            }
            return RedirectToAction("Details", new {id = studentId});
        }

        public IActionResult RemoveFromCourse(int studentId, int courseId)
        {
            if (courseId == 0)
            {
                return RedirectToAction("Details", new { id = studentId });
            }
            StudentCourses courses = new StudentCourses();
            Student student = _unitOfWork.StudentRepository.GetById(studentId);
            Course course = _unitOfWork.CourseRepository.GetById(courseId);
            courses.Course = course;
            courses.CourseId = courseId;
            courses.Student = student;
            courses.StudentId = studentId;
            _unitOfWork.StudenCoursesRepository.Delete(courses);
            _unitOfWork.Save();
            return RedirectToAction("Details", new { id = studentId });
        }
    }
}
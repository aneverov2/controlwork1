﻿using System;
using System.Collections.Generic;
using System.Linq;
using ControlWork1.Data.Repositories.Contracts;
using ControlWork1.Models;
using Microsoft.AspNetCore.Mvc;

namespace ControlWork1.Controllers
{
    public class CourseController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CourseController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IActionResult Index()
        {
            List<Course> courses = _unitOfWork.CourseRepository.GetAll().ToList();
            return View(courses);
        }

        public IActionResult Create(int id)
        {
            if (id == 0)
            {
                return View();
            }

            Course course = _unitOfWork.CourseRepository.GetById(id);
            return View(course);
        }
        public IActionResult CreateOrUpdate(Course course)
        {
            _unitOfWork.CourseRepository.Add(course);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            Course result = _unitOfWork.CourseRepository.GetById(id);
            return View(result);
        }

        public IActionResult DeleteItem(Course course)
        {
            _unitOfWork.CourseRepository.Delete(course);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            Course course = _unitOfWork.CourseRepository.GetById(id);
            return View(course);
        }
    }
}
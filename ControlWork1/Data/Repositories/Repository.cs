﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork1.Data.Repositories.Contracts;
using ControlWork1.Models;
using Microsoft.EntityFrameworkCore;

namespace ControlWork1.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected ApplicationDbContext _context;
        protected DbSet<T> DbSet { get; set; }

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(T entity)
        {
            _context.Entry(entity).State = entity.Id == 0 ?
                EntityState.Added :
                EntityState.Modified;
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

        public T GetById(int id)
        {
            return DbSet.FirstOrDefault(e => e.Id == id);
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork1.Data.Repositories.Contracts;
using ControlWork1.Models;

namespace ControlWork1.Data.Repositories
{
    public class CourseRepository : Repository<Course>, ICourseRepository
    {

        public CourseRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Courses;
        }
    }
}

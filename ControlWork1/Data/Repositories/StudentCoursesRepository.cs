﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork1.Data.Repositories.Contracts;
using ControlWork1.Models;
using Microsoft.EntityFrameworkCore;

namespace ControlWork1.Data.Repositories
{
    public class StudentCoursesRepository: Repository<StudentCourses>, IStudenCoursesRepository
    {
        public StudentCoursesRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.StudentCourses;
        }

        public IEnumerable<StudentCourses> GetStudentCourses(int id)
        {
            List<StudentCourses> courses =
                _context.StudentCourses.Include(c => c.Course).Where(s => s.StudentId == id).ToList();
            return courses;
        }
        public IEnumerable<StudentCourses> GetCoursesWithStudents(int id)
        {
            List<StudentCourses> students =
                _context.StudentCourses.Include(s => s.Student).Where(c => c.CourseId == id).ToList();
            return students;
        }

        public bool IsExist(int studentId, int courseId)
        {
            bool res = false;
            StudentCourses course =
                _context.StudentCourses.FirstOrDefault(c => c.CourseId == courseId && c.StudentId == studentId);
            if (course != null)
            {
                res = true;
            }
            return res;
        }
    }
}

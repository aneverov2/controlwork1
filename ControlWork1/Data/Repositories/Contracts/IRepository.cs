﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork1.Models;

namespace ControlWork1.Data.Repositories.Contracts
{
    public interface IRepository<T> where T : Entity
    {
        void Add(T entity);
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Delete(T entity);
    }
}

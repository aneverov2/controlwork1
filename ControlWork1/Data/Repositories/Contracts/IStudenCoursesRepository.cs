﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork1.Models;

namespace ControlWork1.Data.Repositories.Contracts
{
    public interface IStudenCoursesRepository : IRepository<StudentCourses>
    {
        IEnumerable<StudentCourses> GetStudentCourses(int id);
        IEnumerable<StudentCourses> GetCoursesWithStudents(int id);
        bool IsExist(int studentId, int courseId);
    }
}

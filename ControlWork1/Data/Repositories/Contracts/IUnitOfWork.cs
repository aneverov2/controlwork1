﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork1.Data.Repositories.Contracts
{
    public interface IUnitOfWork
    {
        IStudentRepository StudentRepository { get; }
        ICourseRepository CourseRepository { get; }
        IStudenCoursesRepository StudenCoursesRepository { get; }

        void Save();
        void RejectChanges();
        void Dispose();
    }
}
